# SAE_Install_2022

**Ce README doit être lu en parallèle avec le diaporama du projet pour avoir des illustrations permettant d'accompagner ce guide et d'autres installations sous linux.**



**MON CHOIX D'INSTALLATION**

    - J'ai choisi de faire un dual boot sur ma clé pour installer Xubuntu sur mon ordinateur. Ce qui me permet donc au démarrage de mon ordinateur de choisir mon système d'exploitation (ici Xubuntu ou Windows). Je trouve cette disposition/distinction plus intéressante qu'une virtual box.

**_COMMENT INSTALLER XUBUNTU SUR SON ACER ASPIRE 3 AVEC UN DUAL BOOT_**

    1. Se munir d'une clé:
        - C'est sur celle-ci que nous allons installer Xubuntu

    2. Installer Rufus et Xubuntu:
        - Vous devrez ensuite installer Rufus et l'ISO de xubuntu: En effet Rufus va rendre votre clé USB "démarrable".
        
**_Liens:_**
        _Xubuntu:_ https://xubuntu.org/download/
        _Rufus:_ https://rufus.ie/fr/

**Car notre but est de redémarrer notre ordinateur sur notre clé USB contenant Xubuntu, ce qui va nous permettre de l'installer sur notre machine.**

_RAPPEL: Aidez-vous du diaporama_

    3. Ouvrez donc Rufus pour "Booter" votre clé:
        - Sélectionnez d'abord le nom de votre clé (Attention il ne faut surtout pas se tromper car cette opération formate votre clé)
        - Puis choisissez l'ISO Xubuntu dans la section correspondante. Il ne vous reste plus grand chose à faire, appuyer sur démarrer.

**C'est fait ! Votre clé est démarrable il suffit juste de redémarrer votre machine sur cette dernière !**

**Cependant c'est ici que les subtilités d'un acer ASPIRE 3 retrent en jeu.**

    4. Rendez-vous sur le diapo du projet se nommant "Install-party" à la page 3 pour savoir comment redémarrer sur sa clé.

**Une fois que vous êtes arrivés à l'étape du choix du périphérique c'est ici que les subtilités d'un ACER ASPIRE 3 rentre en jeu. En effet vous n'allez pas reconnaitre votre clé car elle aura changeé de nom et sera devenu "Linpus Lite". C'est donc celle-ci que vous choisirez.**

**ATTENTION ! À PARTIR D'ICI VEUILLEZ SUIVRE À LA LETTRE LES INSTRUCTIONS SUIVANTES POUR ÉVITER TOUT PROBLÈME !**

**Le message d'erreur suivant va s'afficher: _SCECURE BOOT FAIL_. Ne paniquez pas, c'est tout à fait normal. Par défaut une sécurité est activée au boot. Notre but est donc le suivant: Enlever cette securité pour installer Xubuntu une bonne fois pour toute. Pour cela il va falloir se rendre sur ce qu'on appelle le BIOS de votre machine et y créer un mot de passe que l'on rentrera lors d'un deuxième redémarrage de votre machine pour y débloquer plusieurs fonctionnalités/réglages/options. Vous l'aurez donc compris, la désactivation du _secure boot_ en fait partie. PLACE AU ÉTAPE !**

    1. Lorsque vous vous trouvez sur le message d'erreur, appuyer sur la touche "ENTRÉE". Cela démarrera automatiquement Windows.

    2. Vous allez maintenant redémarrez votre ordinateur:
        - À partir d'ici vous allez appuyez sur la touche F2 de votre machine jusqu'à que le BIOS apparaisse.

    3. Une fois sur le BIOS vous ne pouvez plus utiliser votre souris, vous vous déplacerez à l'aide des flèches directionnelles de votre clavier:
        - Rendez-vous dans l'onglet SECURITY. Comme vous n'avez pas de mot de passe, toutes les fonctionnalités sont bloquées sauf une; la création de celui-ci. Appuyez sur ENTRÉE et créez votre mot de passe.
        - Rendez-vous cette fois ci dans l'onglet "EXIT". Sélectionnez l'option suivante: "SAVE CHANGES AND EXIT".

**Cela va donc redémarrer Windows. Nous voici presque à la fin ! Nous allons redémarrer une seconde fois notre machine et entrer dans le BIOS et désactiver le _secure boot_ !**

    1. Entrez dans votre BIOS. Cette fois-ci votre mot de passe vous sera demandé, vous n'aurez qu'à l'entrer.

    2. Vous voilà dans VOTRE BIOS ! Rendez-vous dans l'onglet "BOOT":
        - Dirigez-vous vers l'option "Secure Boot". Comme vous pouvez le voir elle est activée ("Enabled"), appuyer sur "ENTRÉE et choisissez la valeur "Disabled". Quittez maintenant votre BIOS de la même façon que précédemment.

**Vous pouvez donc maintenant redémarrer votre ordinateur sur votre clé sans problème. Plusieurs option vous seront proposées, choisissez "INSTALLER XUBUNTU (safe graphics)". Vous pouvez vous rendre sur l'étape 5 de la page 4 du diapo "Install-party" pour avoir un apercu de l'affichage des choix.**

**Arrivé ici il vous faudra juste suivre les instructions d'installation (sauf pour la configuration du réseau internet que vous devez ignorer) et appuyer sur suivant. _CEPENDANT !_ Dernière subtilité extrêmement importante. Il vous sera demandé si vous voulez installer Xubuntu en parallèle de Windows (Pour pouvoir jongler entre les deux) ou complètement remplacer Windows par Xubuntu. C'est bien évidemment la première option qui nous intéresse et qu'il faudra choisir au risque de perdre TOUTES vos données Windows ! Lorsque vous voudrez retourner sur Windows, il vous suffira de redémarrer votre ordinateur et de choisir Windows comme système d'exploitation et vice versa !**

**_FÉLICITATION ! VOUS AVEZ FINI VOTRE INSTALLATION oui !_**


